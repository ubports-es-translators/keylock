import QtQuick 2.7
import Ubuntu.Components 1.3 as UITK
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import QtQuick.Controls.Suru 2.2
import Ubuntu.Content 1.3
import "../backend" 1.0


ApplicationWindow {
    id: root
    objectName: 'mainView'

    width: units.gu(45)
    height: units.gu(75)

    Suru.theme: Suru.System

    StackView {
        id: pageStack
        anchors {
            fill: parent
            bottomMargin: UbuntuApplication.inputMethod.visible ? UbuntuApplication.inputMethod.keyboardRectangle.height/(units.gridUnit / 8) : 0
            Behavior on bottomMargin {
                NumberAnimation {
                    duration: 175
                    easing.type: Easing.OutQuad
                }
            }
        }

        Component.onCompleted: pageStack.push("pages/KeyList.qml")
    }

    Keylock {
        id: keylock
    }

    Connections {
        target: ContentHub
        onShareRequested: {
//            var filePath = String(transfer.items[0].url).replace('file://', '')
            
            pageStack.push("pages/ImportExportPage.qml", {"url": String(transfer.items[0].url)});
        }
    }
}
