import QtQuick 2.7
import Ubuntu.Components 1.3 as UITK
import Ubuntu.Components.Popups 1.3 as UITK_Popups
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import "../delegates"
import "../components"

Page {
    id: keyListPage

    header: UITK.PageHeader {
        id: header
        title: i18n.tr('Enigma')

        trailingActionBar.actions: [
            UITK.Action {
                id: addKey
                objectName: "addKey"
                text: i18n.tr("Add key")
                iconName: "add"
                onTriggered: {
                    UITK_Popups.PopupUtils.open(addKeyDialog)
                }
            }
        ]
    }

    ScrollView {
        anchors.fill: parent

        ListView {
            id: keyListView
            anchors.fill: parent

            model: keylock.list

            delegate: KeyListItem {
                key: modelData
                onRemoveKey: {
                    UITK_Popups.PopupUtils.open(removeConfirmationComponent)
                }

               Component {
                    id: removeConfirmationComponent
                    ConfirmationPopup {
                        onConfirmed: keylock.removeKey(key)
                        text: i18n.tr("Do you really want to delete the key for ") + key.uids.toString() + i18n.tr("?")
                    }
                }
            }
        }
    }

    AddKeyDialog {
        id: addKeyDialog
    }

 
}

