# Enigma

A PGP app for UBuntu Touch

## Build for and run on desktop

Replace


```
    gpg = gnupg.GPG(gnupghome='/home/phablet/.local/share/enigma.hummlbach/keys',
                    gpgbinary='/opt/click.ubuntu.com/enigma.hummlbach/current/bin/gpg') 
```

by 

```
    gpg = gnupg.GPG(gnupghome='/home/phablet/.local/share/enigma.hummlbach/keys')
```

and run

```
clickable --config clickable-desktop.json desktop
```
