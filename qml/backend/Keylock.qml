import QtQuick 2.6
import io.thp.pyotherside 1.3

Python {
    id: keylock
    property var list: null
    property var currentKey: null
    property var encryptedFile: null
    property var decryptedFile: null
    property var passphrase: null

    // how to these readonly
    property bool ongoingEncryption: false
    property bool ongoingDecryption: false

    function generateKey(email, passphrase) {
    /* TODO: the name overloading here is a bit confusing =) */
        keylock.call('keylock.generate_key', [email, passphrase], onKeyGenerated);
    }

    function onKeyGenerated(keylist) {
        currentKey = keylist[keylist.length-1];
        listKeys(keylist);
    }

    function listKeys(keyList) {
        keylock.list = keyList;
    }

    function removeKey(key) {
        keylock.call('keylock.remove_key', [key.fingerprint], listKeys);
    }

    onEncryptedFileChanged: {
        if (encryptedFile && !ongoingEncryption) {
            ongoingDecryption = true;
            keylock.call('keylock.decrypt', [encryptedFile, passphrase], onDecrypted);
        }
    }
    
    function onDecrypted(filename) {
        decryptedFile = filename;
        ongoingDecryption = false;
    }

    onDecryptedFileChanged: {
        if (decryptedFile && !ongoingDecryption) {
            ongoingEncryption = true;
            keylock.call('keylock.encrypt', [decryptedFile, currentKey], onEncrypted);
        }
    }

    function onEncrypted(filename) {
        encryptedFile = filename;
        ongoingEncryption = false;
    }

    function importKey(filename) {
        keylock.call('keylock.import_key', [filename], onKeyImported);
    }

    function onKeyImported(keylist) {
        currentKey = keylist[keylist.length-1];
        listKeys(keylist);
    }

    Component.onCompleted: {
        addImportPath(Qt.resolvedUrl('../../py'));
        importModule('keylock', onModuleImported);
    }

    function onModuleImported() {
        keylock.call('keylock.list_keys', [], listKeys);
    }

    function isKeyfile(filename) {
        var ext = filename.substr(filename.lastIndexOf('.') + 1);
        return (ext == 'asc');
    }

    function isEncrypted(filename) {
        return keylock.call_sync('keylock.is_encrypted', [filename]);
    }

    onError: {
        console.log('python error: ' + traceback);
    }
}

