import gnupg
import os
import re


def gpg_instance():
    if not os.path.exists('/home/phablet/.local/share/enigma.hummlbach/keys'):
        os.makedirs('/home/phablet/.local/share/enigma.hummlbach/keys')
    gpg = gnupg.GPG(gnupghome='/home/phablet/.local/share/enigma.hummlbach/keys')
    return gpg


def generate_key(email, password):
    gpg = gpg_instance()
    input_data = gpg.gen_key_input(key_type="RSA",
                                   key_length=4096,
                                   name_email=email,
                                   passphrase=password)
    key = gpg.gen_key(input_data)
    return list_keys()


def list_keys(privateOnly=False):
    gpg = gpg_instance()
    return gpg.list_keys(privateOnly)


def remove_key(fingerprint):
    gpg = gpg_instance()
    gpg.delete_keys(fingerprint, True)
    gpg.delete_keys(fingerprint)
    return list_keys()


def encrypt(filename, key):
    gpg = gpg_instance()
    with open(filename, 'rb') as in_stream:
        encrypted_data = gpg.encrypt_file(in_stream, [email(key)], output=filename+'.pgp.txt')
        if not encrypted_data.ok:
            print(encrypted_data.status)
        in_stream.close()
        return filename+'.pgp.txt'

def email(key):
    match = re.search(r'[\w\.-]+@[\w\.-]+', key['uids'][0])
    return match.group(0)


def decrypt(filename, passphrase=None):
    gpg = gpg_instance()
    if not passphrase:
        passphrase = None
    if filename[-4:] == '.pgp':
        out_filename = filename[:-4]
    elif filename[-8:] == '.pgp.txt':
        out_filename = filename[:-8]
    else:
        root, extension = os.path.splitext(filename)
        out_filename = root+'.decrypted'+extension
    with open(filename, 'rb') as in_stream:
        decrypted_data = gpg.decrypt_file(in_stream, passphrase=passphrase, output=out_filename)
        if not decrypted_data.ok:
            print(decrypted_data.status)
        in_stream.close()
        return out_filename


def import_key(filename):
    gpg = gpg_instance()
    with open(filename, 'rb') as in_stream:
        key_data = in_stream.read()
        gpg.import_keys(key_data)
        in_stream.close()
        return list_keys()

def is_encrypted(filename):
    with open(filename, 'r') as stream:
        line = stream.readline()
        stream.close()
        print(line)
        if 'BEGIN PGP MESSAGE' in line:
            return True
        return False
